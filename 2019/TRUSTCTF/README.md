나중에 푼 것들 임니다..

![](img/Rank.png)

# REV

## MESS (100P)
completely mess.
Author : Edward(이규형) 문의는 m0nd2y(이동준)에게 부탁드립니다.

![](img/Mess_1.jpg)
호퍼로 뜯어보고 수상하게 생긴 놈 발견!
```S3CRe7PA5sW0rD```

![](img/Mess_2.jpg)
그대로 ```Mess.exe```에게 입력해줬더니

```flag: TRUST{bBRWt>UHD?5wQ}```

## 나를크랙! (472P)
٩( ᐛ )و

Author : 생선스프 (문의 : m0nday)

![](img/CrackME.jpg)
프로그램 킬때 알아서 Flag가 캡챠보드에 들어갔다.

```flag: TRUST{W0w_You_F!ind_IT}```

# MISC

## MIC CHECK! (100P)
1st TRUST CTF에 오신걸 환영합니다!

Notifications과 Rules를 반드시 읽어주세요!

디스코드(IRC) : https://discord.gg/ZYyupm8

Flag : TRUST{Welcome_CTF_Have_FUN!}

```flag: TRUST{Welcome_CTF_Have_FUN!}```

## Easy Taebo (100P)
TRUST CTF에서도 태.보.해.

nc server.trustctf.com 44923

Author : st4nw(조정훈)
```py
from pwn import *

# Server is UP State
p = remote("server.trustctf.com", 44923)
sleep(4) # 4 Seconds Wait
print("Success") # Sleep Next HI Message Show

# for mon!
for i in range(1,101):
  filter = p.recvuntil("Taebo " + str(i) + " :")
  print (filter)
  filter2 = p.recv().replace("left_mid_jab", "@=(^0^)@").replace("right_mid_jab", "@(^0^)=@").replace("left_jab", "@==(^0^)@").replace("mid_jab", "@(^0^)@").replace("right_jab", "@(^0^)==@").replace("left_hook", "@(^0^)@==").replace("right_hook", "==@(^0^)@").replace("left_speedball", "@@@(^0^)").replace("right_speedball", "(^0^)@@@").replace("left_kick", "@||(^0^)==@").replace("mid_kick", "@==(^||^)==@").replace("right_kick", "@==(^0^)||@").replace(" + ", " ").replace(">>", "")
  print (filter2)
  filter3 = p.send(filter2.lstrip())
  #print (filter3)

pororo = p.recv(2048)
print (pororo)
```

```flag: TRUST{w0w_y0u_9o7_4_w0nd3rfu1_b0dy_lik3_m3}```